import $ from "/wp-content/plugins/infocob-configu/dist/jquery/jquery.js";
import * as jsonElements from '/wp-content/plugins/infocob-configu/js/core/core_JsonElements.js';
import * as resetAll from '/wp-content/plugins/infocob-configu/js/core/core_reset.js';
import * as procesHTML from '/wp-content/plugins/infocob-configu/js/core/core_HTMLprocess.js';
import * as exportConfig from '/wp-content/plugins/infocob-configu/js/core/core_export.js';
import * as displayAll from '/wp-content/plugins/infocob-configu/js/core/core_displayAll.js';
import * as importConfig from '/wp-content/plugins/infocob-configu/js/core/core_import.js';
import * as sendMailConfig from '/wp-content/plugins/infocob-configu/js/core/core_mail.js';
import {version} from "/wp-content/plugins/infocob-configu/js/main.js";

export const rootURL = window.location.protocol + "//" + location.hostname;

/**
 * Get a new configuration id
 * @return {Promise<unknown>}
 */
export function getNewId(){
    return new Promise((resolve, reject) => {
        $.get(rootURL +"/wp-content/plugins/infocob-configu/inc/core/getLastConfigId.php", html => {
            const newId = parseInt(html.id) + 1;
            resolve( newId );
        });
    });
}

/**
 * Save current localstorage to server
 */
export function saveConfigu() {
    try {
        $.ajax({
            type: "POST",
            url: "/wp-content/plugins/infocob-configu/inc/core/saveConfigu.php",
            data: {configu: jsonElements.JsonItems()},
            //dataType: 'json',
            //timeout: 12000, //2 Minutes
            //cache: false,
            //contentType: false,
            //processData: false,

            success: function (datas) {
                //success
            },
            error: function (xhr) {
                console.log(xhr);
                console.log(jsonElements.JsonItems());

                if (xhr.status == 422) {
                    console.log("422");
                } else {
                    console.log('An error occurred while processing the request.');
                }
            }
        });
    } catch (e) {
        console.log(e);
    }

}

/**
 * Get current version
 * @return {*}
 */
export function getCurrentVersion(){
    return JSON.parse(localStorage.getItem("core_general")).version.value;
}

/**
 * Get current date
 * @return {string}
 */
export function getdate(){
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    return dd + '/' + mm + '/' + yyyy;
}

/**
 * Change date
 * @param date
 */
export function setDateUpdate(date = getdate()){
    let dateUpdate = JSON.parse(localStorage.getItem("core_general"));

    dateUpdate.date_update.value = date;

    localStorage.setItem("core_general", JSON.stringify(dateUpdate));
}

/**
 * Get configu state
 * @param method
 * @param value
 * @return {string}
 */
export function configState(method = "get", value= ""){

    let state = JSON.parse(localStorage.getItem("core_general"));

    if(method === "get"){
        return state.state.value;
    }

    if(method === "post"){
        state.state.value = value;
        localStorage.setItem("core_general", JSON.stringify(state));
    }
}

/**
 * Get current id
 * @return {number}
 */
export function getCurrentId(){
    if(localStorage.getItem("core_general")){
        let currentId = JSON.parse(localStorage.getItem("core_general")).id.value;
        return parseInt(currentId);
    }else{ 
        getNewId().then(function(newId) {
            return newId;
        });
    }
}

export function importConfiguFromUrl(id){

    $.ajax({
        url: rootURL + "/wp-content/plugins/infocob-configu/inc/backEnd/getAllConfigu.php",
        success: function(data){
            $.each(data["configurations"], function(i, item) {

                if(item.general.id.value === id){
                    //set in current localstorage
                    localStorage.setItem("core_general", JSON.stringify(item.general));
                    localStorage.setItem("core_informations", JSON.stringify(item.informations));
                    localStorage.setItem("mod_logo", JSON.stringify(item.logos));
                    localStorage.setItem("mod_color", JSON.stringify(item.colors));
                    localStorage.setItem("mod_font", JSON.stringify(item.fonts));
                    localStorage.setItem("mod_social", JSON.stringify(item.socials));
                    localStorage.setItem("mod_menu", JSON.stringify(item.menus));
                    localStorage.setItem("mod_contenus", JSON.stringify(item.contents));
                    localStorage.setItem("mod_media", JSON.stringify(item.medias));
                }
            });
        }
    });
}

export function testIfFisrtVisit(){
    if ( !localStorage.getItem("core_general")) {

        resetAll.resetConfig(); //reset to get vanilla config
        procesHTML.closeConf(); //force close popup on first visit
        $("#send_mail_container").removeClass("active");
        $("body").css("overflow", "initial");

        Swal.fire({
            title: '<strong>Bienvenue sur notre <u>configurateur</u></strong>',
            icon: 'info',
            html: `<p>Ce logiciel est actuellement en phase BETA, vous pouvez rencontrer certains bugs.<br>
             En cas de non possibilitée d'utilisation, merci de contacter Infocob ou d'ouvrir cette page sur une page
             de navigation privée.</p><br>
             
             <h3>Marche à suivre -></h3>
             <p>Vous avez la possibilité de customiser cette page afin de pouvoir vous donner un aperçu des possibilités
             des thèmes Infocob. Dès lors votre configuration terminée, vous pouvez envoyer cette configuration à nos
             équipes afin qu'on puisse réaliser votre projet le plus efficacement possible.<p/>
             <br>
             <ul>
                 <li>Ajoutez votre logo</li>
                 <li>Modifier les couleurs</li>
                 <li>Modifier les polices de caractères</li>
                 <li>Sélectionnez vos réseaux sociaux</li>
                 <li>Modifiez le carousel et les élèments de cette page</li>
                 <li>Modifiez le menu</li>
                 <li>Ajoutez les éventuels contenus pour chaques pages</li>
                 <li>Envoyez votre configuration à Inforcob</li>
             </ul>
             <br>
             <p><b>Version :</b><i>${version}</i></p>
             `,
            showCloseButton: true,
            showCancelButton: false,
            focusConfirm: false,
            confirmButtonText: 'Commencer la configuration'
        }).then((result) => {
            /* Read more about isConfirmed */
            if (result.isConfirmed) {
                location.reload();
            }
        });


    } else {

        //test if good version inside localStorage
        if (getCurrentVersion() !== version) {
            exportConfig.exportAll();
            resetAll.pre_resetConfig(true);
        }

        //test if configu was open or not last visit
        if (configState() === "true") {
            procesHTML.openConf();
        } else {
            procesHTML.closeConf();
            $("#send_mail_container").removeClass("active");
            $("body").css("overflow", "initial");
        }
    }
}

export function referenceTest(){
    let url      = window.location.href;
    let arr = url.split('?');
    if (arr.length > 1 && arr[1] !== '') {

        if (arr[1].indexOf('import') > -1) {
            let getParam = url.split('import=');
            let referenceImported = getParam[1];
            if(referenceImported){
                let getIdFromReference = referenceImported.split('_');
                let removeFirstString = getIdFromReference[0].split('confi');
                let id = removeFirstString[1];

                if(id){
                    importConfiguFromUrl(id);
                }
            }
        }
    }
}

export function checkIfUserConnected(){
    $.ajax({
        url: rootURL + "/wp-content/plugins/infocob-configu/inc/core/checkUserConnected.php",
        success: function(data){
            if(data === "1"){
                configuScript("admin");
            }else{
                configuScript("customer");
            }
        }
    });
}

export function configuScript(user){
    //=================-----Html to generate Section------==================//

    if(user === "admin"){
        //------- Wrap All li from configurator
        $(".nav-item-wrap").slideUp();
        //------- Wrap All li from configurator

        //------- Show all Section from configurator with delay
        $(function () {
            function show_nav() {
                $(".popup-demo-psion nav").css('opacity', '1');
            }

            window.setTimeout(show_nav, 1000);
        });
        //------- Show all Section from configurator with delay

        //------- On click configurator item open it
        $(".li_container > header").click(function () {
            $(this).parent().find(".nav-item-wrap").slideToggle().parent().toggleClass('open');
        });
        //------- On click configurator item open it

        $(".button_container_config").append("<i class='version'>" + version + "</i>");

        //mail sender container
        $("body").append("<section id='send_mail_container'></section>");
        $("#send_mail_container").append("<span id='close_mail_container'>x</span>");

        $.get("/wp-content/plugins/infocob-configu/template/mail_form.html", function (html_string) {
            $("#send_mail_container").append(html_string);
        }, 'html');    // this is the change now its working

        //=================-----End Html to generate Section------==================//


        //=================-----Start Algorithm Section------==================//
        //Test if first visit
        testIfFisrtVisit();

        //close / open configu
        $(".popupOpenConf").click(function () {
            if (configState() === "true") {
                configState("post", "false");

                procesHTML.closeConf();
                $("#send_mail_container").removeClass("active");
                $("body").css("overflow", "initial");
            } else {
                configState("post", "true");
                procesHTML.openConf();
            }
        });


        //add a reference and a commercial
        async function addRef(getValueGen) {
            const {value: formValues} = await Swal.fire({
                title: 'Initialisation de la configuration',
                html:
                    '<div class="form_swal">' +
                    '<label for="getInputReference">Ajoutez votre référence</label>' +
                    '<input id="getInputReference" class="swal2-input">' +
                    '</div>' +

                    '<div class="form_swal">' +
                    '<label for="getInputCommercial">Le commercial lié</label>' +
                    '<input id="getInputCommercial" class="swal2-input">' +
                    '</div>',
                focusConfirm: false,
                preConfirm: () => {
                    return [
                        document.getElementById('getInputReference').value,
                        document.getElementById('getInputCommercial').value
                    ]
                }
            });

            if (formValues) {

                let newReference = "confi" + getValueGen.id.value + "_" + formValues[0];
                let newCommercial = formValues[1];

                getValueGen.reference.value = newReference;
                getValueGen.commercial.value = newCommercial;

                localStorage.setItem("core_general", JSON.stringify(getValueGen));
                $("#display_reference").append('<b>Référence : </b>' + newReference);
                $("#display_commercial").append('<b>Commercial lié : </b>' + newCommercial);
            }
        }

        let getValueGen = jQuery.parseJSON(localStorage.getItem("core_general"));

        if (getValueGen.reference.value === "") {
            addRef(getValueGen);
        }

        if (getValueGen.reference.value !== "" && getValueGen.commercial.value !== "") {
            $("#display_reference").append('<b>Référence : </b>' + getValueGen.reference.value);
            $("#display_commercial").append('<b>Commercial lié : </b>' + getValueGen.commercial.value);
        }

        //Display contents
        displayAll.getConfigLS();

        //=================-----End Start Algorithm Section------==================//


        //=================-----Export Section------==================//
        //export in csv on click
        $(".save_configurateur").click(function () {
            exportConfig.exportAll();
        });
        //=================-----End Export Section------==================//

        //=================-----Start Import Section------==================//
        $('.charge_save_container').on('change', '#getsaveConfig', function () {
            importConfig.pre_importConfig();
        });
        //=================-----End Import Section------==================//


        //=================-----Mail Section------==================//
        //onclick start send mail
        $(".sendMailConfig").click(async function () {
            sendMailConfig.mail_sender();
        });
        //=================-----End Mail Section------==================//


        //=================-----Reset Section------==================//
        $(".reset_configurateur").click(function () {
            resetAll.pre_resetConfig();
        });
        //=================-----End Reset Section------==================//
    }

    if(user === "customer"){
        $(".popup-demo-psion").hide();
        $(".popupOpenConf").hide();

        //Display contents
        displayAll.getConfigLS();
    }
}
