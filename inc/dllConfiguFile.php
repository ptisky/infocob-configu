<?php

/**
 * Download configu file
 */

// Initialize a file URL to the variable
$url = '../configurations/configuration_' . $_GET["id"] . '/configu.json';

// Use basename() function to return the base name of file
$file_name = basename($url);

// Use file_get_contents() function to get the file
// from url and use file_put_contents() function to
// save the file by using base name
if (file_put_contents($file_name, file_get_contents($url))) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($url) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($url));
    flush(); // Flush system output buffer
    readfile($url);
    die();
} else {
    var_dump($url);
    echo "File downloading failed.";
}