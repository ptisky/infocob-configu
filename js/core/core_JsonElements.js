/**
 * Get all localstorage
 * @return {{general: any, medias: any, fonts: any, contents: any, menus: any, socials: any, logos: any, colors: any, informations: any}}
 * @constructor
 */
export function JsonItems() {
    return {
        "general": JSON.parse(localStorage.getItem("core_general")),
        "informations": JSON.parse(localStorage.getItem("core_informations")),
        "logos": JSON.parse(localStorage.getItem("mod_logo")),
        "colors": JSON.parse(localStorage.getItem("mod_color")),
        "fonts": JSON.parse(localStorage.getItem("mod_font")),
        "socials": JSON.parse(localStorage.getItem("mod_social")),
        "menus": JSON.parse(localStorage.getItem("mod_menu")),
        "contents": JSON.parse(localStorage.getItem("mod_contenus")),
        "medias": JSON.parse(localStorage.getItem("mod_media"))
    };
}