<?php
/*
Plugin Name: Infocob Configu
Plugin URI: https://www.infocob-web.com/
Description: Configurateur pour les themes démo Infocob web
Author: Infocob
Version: 1.0
Author URI: https://www.infocob-web.com/
*/

$version = 1.0;

//*** FRONT ***//
function type_module_js($tag, $handle, $src)
{
    if ('main-configu-js' === $handle) {
        $tag = '<script type="module" src="' . esc_url($src) . '"></script>';
    }
    return $tag;
}

function configu_Script()
{
    //sweetAlert2
    wp_enqueue_script('sweetalert2-js', '/wp-content/plugins/infocob-configu/dist/sweetalert2/sweetalert2.all.min.js');
    wp_enqueue_style('sweetalert2-css', '/wp-content/plugins/infocob-configu/dist/sweetalert2/sweetalert2.min.css');

    //dropzone
    wp_enqueue_script('dropzone-js', 'https://unpkg.com/dropzone@5.9.3/dist/min/dropzone.min.js');
    wp_enqueue_style('dropzone-css', 'https://unpkg.com/dropzone@5.9.3/dist/min/dropzone.min.css');

    //FontAwesome
    wp_enqueue_style('fontAwesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css');


    wp_enqueue_style('wp-color-picker');


    // Manually enqueing Iris itself by linking directly
    //    to it and naming its dependencies
    wp_enqueue_script(
        'iris',
        admin_url('js/iris.min.js'),
        array(
            'jquery-ui-draggable',
            'jquery-ui-slider',
            'jquery-touch-punch',
            'jquery-ui-sortable'
        ),
        false,
        1
    );

    // Now we can enqueue the color-picker script itself,
    //    naming iris.js as its dependency
    wp_enqueue_script(
        'wp-color-picker',
        admin_url('js/color-picker.min.js'),
        array('iris'),
        false,
        1
    );

    // Manually passing text strings to the JavaScript
    $colorpicker_l10n = array(
        'clear' => __('Clear'),
        'defaultString' => __('Default'),
        'pick' => __('Select Color'),
        'current' => __('Current Color'),
    );
    wp_localize_script(
        'wp-color-picker',
        'wpColorPickerL10n',
        $colorpicker_l10n
    );

    //main.js
    wp_enqueue_script('main-configu-js', '/wp-content/plugins/infocob-configu/js/main.js');
    wp_enqueue_style('main-configu-css', '/wp-content/plugins/infocob-configu/css/main.css', [], '1.0.1');
}


function displayHTMLpage()
{
    $asubHTML = file_get_contents(ABSPATH . 'wp-content/plugins/infocob-configu/template/configu.html');
    echo $asubHTML;
}


add_action('hook_infocob-configu', 'configu_Script', 100);
add_action('hook_infocob-configu', 'displayHTMLpage', 1);
add_filter('script_loader_tag', 'type_module_js', 10, 3);


//*********************//

//*** BACK ***//
function configu_Script_backend()
{
    //sweetAlert2
    wp_enqueue_script('sweetalert2-js', '/wp-content/plugins/infocob-configu/dist/sweetalert2/sweetalert2.all.min.js');
    wp_enqueue_style('sweetalert2-css', '/wp-content/plugins/infocob-configu/dist/sweetalert2/sweetalert2.min.css');

    //bootstrap
    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js');
    wp_enqueue_style('bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');

    //dataTable
    wp_enqueue_script('dataTable-js', 'https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js');
    wp_enqueue_script('dataTable-button-js', 'https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js');
    wp_enqueue_script('dataTable-select-js', 'https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js');

    wp_enqueue_style('dataTable-css', 'https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css');
    wp_enqueue_style('dataTable-buttons-css', 'https://cdn.datatables.net/buttons/2.0.0/css/buttons.dataTables.min.css');
    wp_enqueue_style('dataTable-select-css', 'https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css');
    //wp_enqueue_script('dataTable-js', 'https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js');
    //wp_enqueue_style('dataTable-css', 'https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css');

    //main backend
    wp_enqueue_script('main-configu-js', '/wp-content/plugins/infocob-configu/js/mainBack.js');
    wp_enqueue_style('main-configu-css', '/wp-content/plugins/infocob-configu/css/main.css', [], '1.0.1');


}

function custom_menu()
{
    add_menu_page(
        'Infocob Configu',
        'Infocob Configu',
        'edit_posts',
        'configu_settings',
        'configu_callback_function',
        'dashicons-cover-image'

    );
}

function configu_callback_function($version)
{
    do_action("hook_infocob-configu_backend_top");
    echo "<div id='backend_HTML'></div>";
}

add_action('hook_infocob-configu_backend_top', 'configu_Script_backend', 2);
add_action('admin_menu', 'custom_menu');
//*********************//


?>