//==--- All demos ---==//
import * as demo1 from '/wp-content/plugins/infocob-configu/js/demo/demo1.js';
import * as demo2 from '/wp-content/plugins/infocob-configu/js/demo/demo2.js';
import * as demo3 from '/wp-content/plugins/infocob-configu/js/demo/demo3.js';
import * as demo4 from '/wp-content/plugins/infocob-configu/js/demo/demo4.js';
import * as demo5 from '/wp-content/plugins/infocob-configu/js/demo/demo5.js';
//==--- End All demos ---==//

//==--- LocalStorage Default values ---==//
export let default_logo;
export let default_color;
export let default_font;
export let default_social;
export let default_menu;
export let default_contenus;
export let default_media;
//==--- End LocalStorage Default values ---==//

//==--- DOM Default values ---==//
export let dom_logo_header_source;
export let dom_logo_header_img;
export let dom_logo_footer_source;
export let dom_logo_footer_img;
//==--- End DOM Default values ---==//

const getCurrentDemolocation = window.location.hostname;
console.log(getCurrentDemolocation);
if (getCurrentDemolocation === "demo1.local" || getCurrentDemolocation === "demo1.infocob-web.com") {

    //==--- LocalStorage Default values ---==//
    default_logo = demo1.default_logo;
    default_color = demo1.default_color;
    default_font = demo1.default_font;
    default_social = demo1.default_social;
    default_menu = demo1.default_menu;
    default_contenus = demo1.default_contenus;
    default_media = demo1.default_media;
//==--- End LocalStorage Default values ---==//

    //==--- DOM Default values ---==//
    dom_logo_header_source = demo1.dom_logo_header_source;
    dom_logo_header_img = demo1.dom_logo_header_img;
    dom_logo_footer_source = demo1.dom_logo_footer_source;
    dom_logo_footer_img = demo1.dom_logo_footer_img;
    //==--- End DOM Default values ---==//
}

if (getCurrentDemolocation === "demo2.infocob-web.com") {

    //==--- LocalStorage Default values ---==//
    default_logo = demo2.default_logo;
    default_color = demo2.default_color;
    default_font = demo2.default_font;
    default_social = demo2.default_social;
    default_menu = demo2.default_menu;
    default_contenus = demo2.default_contenus;
    default_media = demo2.default_media;
//==--- End LocalStorage Default values ---==//

    //==--- DOM Default values ---==//
    dom_logo_header_source = demo2.dom_logo_header_source;
    dom_logo_header_img = demo2.dom_logo_header_img;
    dom_logo_footer_source = demo2.dom_logo_footer_source;
    dom_logo_footer_img = demo2.dom_logo_footer_img;
    //==--- End DOM Default values ---==//
}

if (getCurrentDemolocation === "demo3.infocob-web.com") {

    //==--- LocalStorage Default values ---==//
    default_logo = demo3.default_logo;
    default_color = demo3.default_color;
    default_font = demo3.default_font;
    default_social = demo3.default_social;
    default_menu = demo3.default_menu;
    default_contenus = demo3.default_contenus;
    default_media = demo3.default_media;
//==--- End LocalStorage Default values ---==//

    //==--- DOM Default values ---==//
    dom_logo_header_source = demo3.dom_logo_header_source;
    dom_logo_header_img = demo3.dom_logo_header_img;
    dom_logo_footer_source = demo3.dom_logo_footer_source;
    dom_logo_footer_img = demo3.dom_logo_footer_img;
    //==--- End DOM Default values ---==//
}

if (getCurrentDemolocation === "demo4.infocob-web.com") {

    //==--- LocalStorage Default values ---==//
    default_logo = demo4.default_logo;
    default_color = demo4.default_color;
    default_font = demo4.default_font;
    default_social = demo4.default_social;
    default_menu = demo4.default_menu;
    default_contenus = demo4.default_contenus;
    default_media = demo4.default_media;
//==--- End LocalStorage Default values ---==//

    //==--- DOM Default values ---==//
    dom_logo_header_source = demo4.dom_logo_header_source;
    dom_logo_header_img = demo4.dom_logo_header_img;
    dom_logo_footer_source = demo4.dom_logo_footer_source;
    dom_logo_footer_img = demo4.dom_logo_footer_img;
    //==--- End DOM Default values ---==//
}

if (getCurrentDemolocation === "demo5.infocob-web.com") {

    //==--- LocalStorage Default values ---==//
    default_logo = demo5.default_logo;
    default_color = demo5.default_color;
    default_font = demo5.default_font;
    default_social = demo5.default_social;
    default_menu = demo5.default_menu;
    default_contenus = demo5.default_contenus;
    default_media = demo5.default_media;
//==--- End LocalStorage Default values ---==//

    //==--- DOM Default values ---==//
    dom_logo_header_source = demo5.dom_logo_header_source;
    dom_logo_header_img = demo5.dom_logo_header_img;
    dom_logo_footer_source = demo5.dom_logo_footer_source;
    dom_logo_footer_img = demo5.dom_logo_footer_img;
    //==--- End DOM Default values ---==//
}