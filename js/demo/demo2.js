import $ from '/wp-content/plugins/infocob-configu/dist/jquery/jquery.js';

//==--- LocalStorage Default values ---==//

export const default_logo = {
    header: {value: $(".logo-container img").attr("src")},
    footer: {value: $("#footer .footer-logo img").attr("src")}
};

export const default_color = {
    main: {value: "#e3ac06"},
    "main-light": {value: "#ebc655"},
    "main-dark": {value: "#b0902f"},
    secondary: {value: "#000000"},
    grey: {value: "#f0f0f0"},
    text: {value: "#211e1f"},
    header: {value: "#211e1f"},
    'header-shrink': {value: "#211e1f"},
    footer: {value: "#211e1f"},
};

export const default_font = {
    main: {value: eval(getComputedStyle(document.documentElement).getPropertyValue('--infocobweb-font-main'))},
    sec: {value: eval(getComputedStyle(document.documentElement).getPropertyValue('--infocobweb-font-sec'))}
}

export const default_social = {
    facebook: {value: "true"},
    twitter: {value: "true"},
    youtube: {value: "true"},
    linkedin: {value: "true"},
    instagram: {value: "false"},
    pinterest: {value: "false"},
    houzz: {value: "false"},
}

export const default_menu = {
    main: {
        0: {
            'id': 1,
            'name': 'Accueil',
            'position': 1,
            'state': "checked",
            'default': 1,
            'url': '/'
        },
        1: {
            'id': 2,
            'name': 'Le restaurant',
            'position': 2,
            'state': "checked",
            'default': 1,
            'url': '/le-restaurant'
        },
        2: {
            'id': 3,
            'name': 'La carte',
            'position': 3,
            'state': "checked",
            'default': 0,
            'url': '/la-carte'
        },
        3: {
            'id': 4,
            'name': 'Le burger du mois',
            'position': 4,
            'state': "checked",
            'default': 0,
            'url': '/le-burger-du-mois'
        },
        4: {
            'id': 5,
            'name': 'Actualités',
            'position': 5,
            'state': "checked",
            'default': 1,
            'url': '/actualites'
        },
        5: {
            'id': 6,
            'name': 'Recrutement',
            'position': 6,
            'state': "checked",
            'default': 0,
            'url': '/offre-emploi'
        },
        6: {
            'id': 7,
            'name': 'Contact',
            'position': 7,
            'state': "checked",
            'default': 1,
            'url': '/contact'
        },
        7: {
            'id': 8,
            'name': 'Espace client',
            'position': 8,
            'state': "checked",
            'default': 0,
            'url': 'https://espaceclient.infocob-solutions.com'
        },
        8: {
            'id': 9,
            'name': 'Entrées et salades',
            'position': 301,
            'state': "checked",
            'default': 0,
            'url': '/la-carte/entrees-et-salades'
        },
        9: {
            'id': 10,
            'name': 'Les burgers',
            'position': 302,
            'state': "checked",
            'default': 0,
            'url': '/la-carte/les-burgers'
        },
        10: {
            'id': 11,
            'name': 'Menu midi',
            'position': 303,
            'state': "checked",
            'default': 0,
            'url': '/la-carte/menu-midi'
        }
    },
    footer: {
        0: {
            'id': 1,
            'name': 'Accueil',
            'position': 1,
            'state': "checked",
            'default': 1,
            'url': '/'
        },
        1: {
            'id': 2,
            'name': 'Le restaurant',
            'position': 2,
            'state': "checked",
            'default': 0,
            'url': '/le-restaurant'
        },
        2: {
            'id': 3,
            'name': 'La carte',
            'position': 3,
            'state': "checked",
            'default': 0,
            'url': '/la-carte'
        },
        3: {
            'id': 4,
            'name': 'Le burger du mois',
            'position': 4,
            'state': "checked",
            'default': 0,
            'url': '/le-burger-du-mois'
        },
        4: {
            'id': 5,
            'name': 'Actualités',
            'position': 5,
            'state': "checked",
            'default': 0,
            'url': '/actualites'
        },
        5: {
            'id': 6,
            'name': 'Recrutement',
            'position': 6,
            'state': "checked",
            'default': 0,
            'url': '/offre-emploi'
        },
        6: {
            'id': 7,
            'name': 'Contact',
            'position': 7,
            'state': "checked",
            'default': 0,
            'url': '/contact'
        },
        7: {
            'id': 8,
            'name': 'Espace client',
            'position': 8,
            'state': "checked",
            'default': 0,
            'url': 'https://espaceclient.infocob-solutions.com'
        },
        8: {
            'id': 9,
            'name': 'Politique de confidentialité',
            'position': 9,
            'state': "checked",
            'default': 0,
            'url': '/politique-de-confidentialite'
        },
        9: {
            'id': 10,
            'name': 'Mentions légales',
            'position': 10,
            'state': "checked",
            'default': 0,
            'url': '/mentions-legales'
        },
        10: {
            'id': 11,
            'name': 'Plan du site',
            'position': 11,
            'state': "checked",
            'default': 0,
            'url': '/plan-du-site'
        }
    }
}

export const default_contenus = {
    1:{
        name: "Accueil",
        slug: "accueil",
        fill: false,
    },
    2:{
        name: "Le restaurant",
        slug: "le_restaurant",
        fill: false,
    },
    3:{
        name: "La carte",
        slug: "la-carte",
        fill: false,
    },
    12:{
        name: "Entrées et salades",
        slug: "entree_et_salades",
        fill: false,
    },
    13:{
        name: "Les burgers",
        slug: "les_burgers",
        fill: false,
    },
    14:{
        name: "Menu midi",
        slug: "menu_midi",
        fill: false,
    },
    4:{
        name: "Le burger du mois",
        slug: "burger_du_mois",
        fill: false,
    },
    5:{
        name: "Actualités",
        slug: "actualites",
        fill: false,
    },
    6:{
        name: "Recrutement",
        slug: "recrutement",
        fill: false,
    },
    7:{
        name: "Contact",
        slug: "contact",
        fill: false,
    },
    8:{
        name: "Espace client",
        slug: "espace_client",
        fill: false,
    },
    9:{
        name: "Politique de confidentialité",
        slug: "politique_de_confidentialite",
        fill: false,
    },
    10:{
        name: "Mentions légales",
        slug: "mentions_legales",
        fill: false,
    },
    11:{
        name: "Plan du site",
        slug: "plan_du_site",
        fill: false,
    }
}

export const default_media = {
    images : {
        0 : {
            id : 1,
            image : "/wp-content/uploads/2020/08/burger-du-mois.jpg"
        },
        1 : {
            id : 2,
            image : "/wp-content/uploads/2020/08/le-julien-dessert.jpg"
        },
        2 : {
            id : 3,
            image : "/wp-content/uploads/2020/06/reouverture.jpg"
        },
        3 : {
            id : 4,
            image : "/wp-content/uploads/2020/06/nouveau-burger-vegan.jpg"
        },
        4 : {
            id : 5,
            image : "/wp-content/uploads/2020/06/Rectangle-835.jpg"
        },
        5 : {
            id : 6,
            image : "/wp-content/uploads/2020/06/jasmin-schreiber-V2Kw-YC7Cls-unsplash-scaled.jpg"
        },
        6 : {
            id : 7,
            image : "/wp-content/uploads/2020/06/jakub-kapusnak-4hgYULBzVEE-unsplash-scaled.jpg"
        },
        7 : {
            id : 8,
            image : "/wp-content/uploads/2020/06/dan-gold-E6HjQaB7UEA-unsplash-scaled.jpg"
        },
        8 : {
            id : 9,
            image : "/wp-content/uploads/2020/06/yoav-aziz-AiHJiRCwB3w-unsplash-scaled.jpg"
        },
        9 : {
            id : 10,
            image : "/wp-content/uploads/2020/06/sander-dalhuisen-nA6Xhnq2Od8-unsplash-scaled.jpg"
        },
        10 : {
            id : 11,
            image : "/wp-content/uploads/2020/06/pxfuel.com_-scaled.jpg"
        },
        11 : {
            id : 12,
            image : "/wp-content/uploads/2020/06/melissa-walker-horn-NkwNp3ho-Qw-unsplash-scaled.jpg"
        },
        12 : {
            id : 13,
            image : "/wp-content/uploads/2020/06/leo-roza-W1LVAIae3WE-unsplash-scaled.jpg"
        },
        13 : {
            id : 14,
            image : "/wp-content/uploads/2020/06/artur-tumasjan-qnvuhWnK0gk-unsplash-scaled.jpg"
        },
        14 : {
            id : 15,
            image : "/wp-content/uploads/2020/06/armando-ascorve-morales-ypZI_CA91M0-unsplash-scaled.jpg"
        },
        15 : {
            id : 16,
            image : "/wp-content/uploads/2020/06/accueil-infocob-web-restaurant.png"
        },

    },
    videos : {

    }
}

//==--- End LocalStorage Default values ---==//

//==--- DOM Default values ---==//
export const dom_logo_header_source = $('.logo-container source');
export const dom_logo_header_img = $('.logo-container img');
export const dom_logo_footer_source = $('.footer-logo source');
export const dom_logo_footer_img = $('.footer-logo img');
//==--- End DOM Default values ---==//