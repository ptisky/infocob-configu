import $ from '/wp-content/plugins/infocob-configu/dist/jquery/jquery.js';

import * as mod_logo from "/wp-content/plugins/infocob-configu/js/modules/mod_logo.js";
import * as mod_color from "/wp-content/plugins/infocob-configu/js/modules/mod_color.js";
import * as mod_font from "/wp-content/plugins/infocob-configu/js/modules/mod_font.js";
import * as mod_social from "/wp-content/plugins/infocob-configu/js/modules/mod_social.js";
import * as mod_menu from "/wp-content/plugins/infocob-configu/js/modules/mod_menu.js";
import * as mod_contenus from "/wp-content/plugins/infocob-configu/js/modules/mod_contenus.js";
import * as mod_media from "/wp-content/plugins/infocob-configu/js/modules/mod_media.js";

import * as popup from '/wp-content/plugins/infocob-configu/js/extra/extra_popup.js';

import * as main from '/wp-content/plugins/infocob-configu/js/main.js';
import * as functions from '/wp-content/plugins/infocob-configu/js/functions.js';

/**
 * Ask if we really want reset all current config
 * @param errorVersion
 */
export function pre_resetConfig(errorVersion = false) {

    if (errorVersion === false) {
        Swal.fire({
            title: 'Es-tu sur de vouloir faire ça ?',
            text: "Cela supprimera ta configuration en cours",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, continuer',
            cancelButtonText: 'NON ! Annuler'
        }).then((result) => {
            if (result.isConfirmed) {
                resetConfig();
                popup.PopupEvent.fire({title: "Votre configuration précedente à été remis a zéro"});
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                popup.PopupEvent.fire({title: "Vous n'avez pas supprimé votre configuration"});
            }
        });
    } else {
        resetConfig();
        popup.PopupEvent.fire({title: "Votre configuration précedente à été remis a zéro, dû a une erreur de version. Une sauvegarde de secours à été téléchargé."});
    }
}

/**
 * Script to reset general informations
 */
export function reset_generalInformations(){

    functions.getNewId().then(function(newId) {

        localStorage.setItem("core_general", JSON.stringify({
            id: {value: newId.toString()},
            date_creation: {value: functions.getdate()},
            date_update: {value: functions.getdate()},
            mail_send: {value: "false"},
            wordpress_generator: {value: "false"},
            version: {value: main.version},
            tutorial: {value: "false"},
            state: {value: "false"},
            reference: {value: ""},
            commercial: {value: ""}
        }));

        localStorage.setItem("core_informations", JSON.stringify({
            nom_site: {value: ""},
            tel: {value: ""},
            mail: {value: ""},
            addr_1: {value: ""},
            addr_2: {value: ""},
            societe: {value: ""},
            statut: {value: ""},
            rcs: {value: ""},
            naf: {value: ""},
            tva: {value: ""},
            siege: {value: ""},
            replegal: {value: ""}
        }));


        $("#display_reference").html('');
        $("#display_commercial").html('');

        let getValueGen = jQuery.parseJSON(localStorage.getItem("core_general"));
        //Duplicated code -> need to delete and optimize
        async function addRef(getValueGen) {
            const {value: formValues} = await Swal.fire({
                title: 'Initialisation de la configuration',
                html:
                    '<div class="form_swal">' +
                    '<label for="getInputReference">Ajoutez votre référence</label>' +
                    '<input id="getInputReference" class="swal2-input">' +
                    '</div>' +

                    '<div class="form_swal">' +
                    '<label for="getInputCommercial">Le commercial lié</label>' +
                    '<input id="getInputCommercial" class="swal2-input">' +
                    '</div>',
                focusConfirm: false,
                preConfirm: () => {
                    return [
                        document.getElementById('getInputReference').value,
                        document.getElementById('getInputCommercial').value
                    ]
                }
            });

            if (formValues) {

                let newReference = "confi" + getValueGen.id.value + "_" + formValues[0];
                let newCommercial = formValues[1];

                getValueGen.reference.value = newReference;
                getValueGen.commercial.value = newCommercial;

                localStorage.setItem("core_general", JSON.stringify(getValueGen));
                $("#display_reference").append('<b>Référence : </b>' + newReference);
                $("#display_commercial").append('<b>Commercial lié : </b>' + newCommercial);
            }
        }

        if (getValueGen.reference.value === "") {
            addRef(getValueGen);
        }
    });
}

/**
 * Script de to reset logo module
 */
export function reset_mod_logo(){
    mod_logo.resetLogo("header");
    mod_logo.resetLogo("footer");
}

/**
 * Script de to reset color module
 */
export function reset_mod_color(){
    mod_color.resetColor("main");
    mod_color.resetColor("main-light");
    mod_color.resetColor("main-dark");
    mod_color.resetColor("secondary");
    mod_color.resetColor("grey");
    mod_color.resetColor("text");
    mod_color.resetColor("header");
    mod_color.resetColor("header-shrink");
    mod_color.resetColor("footer");
}

/**
 * Script de to reset font module
 */
export function reset_mod_font(){
    mod_font.resetFont("main");
    mod_font.resetFont("sec");
}

/**
 * Script de to reset social module
 */
export function reset_mod_social(){
    mod_social.resetSocial("facebook");
    mod_social.resetSocial("twitter");
    mod_social.resetSocial("youtube");
    mod_social.resetSocial("linkedin");
    mod_social.resetSocial("instagram");
    mod_social.resetSocial("pinterest");
    mod_social.resetSocial("houzz");
}

/**
 * Script de to reset menu module
 */
export function reset_mod_menu(){
    mod_menu.resetMenu("main");
    mod_menu.resetMenu("footer");
}

/**
 * Script de to reset content module
 */
export function reset_mod_content(){
    mod_contenus.resetContenus();
}

/**
 * Script de to reset media module
 */
export function reset_mod_media(){
    mod_media.resetMedia("images");
    mod_media.resetMedia("videos");
}

/**
 * Script de to reset all modules
 */
export function resetConfig() {
    reset_generalInformations();

    reset_mod_logo();
    reset_mod_color();
    reset_mod_font();
    reset_mod_social();
    reset_mod_menu();
    reset_mod_content();
    reset_mod_media();

    functions.saveConfigu();
    popup.PopupSucess("Votre configuration à été réinitialisée avec succès");
}

/**
 * Used to reset module on click in reload button
 */
$(".single_reset").click(function(){
    let element = $(this).attr("data-element");

    if(element === "logo"){
        reset_mod_logo();
        popup.PopupSucess("Les logos ont été réinitialisés avec succès");
    }

    if(element === "color"){
        reset_mod_color();
        popup.PopupSucess("Les couleurs ont été réinitialisées avec succès");
    }

    if(element === "font"){
        reset_mod_font();
        popup.PopupSucess("Les polices ont été réinitialisées avec succès");
    }

    if(element === "social"){
        reset_mod_social();
        popup.PopupSucess("Les reseaux sociaux ont été réinitialisés avec succès");
    }

    if(element === "menu"){
        reset_mod_menu();
        popup.PopupSucess("Les menus ont été réinitialisés avec succès");
    }

    if(element === "content"){
        reset_mod_content();
        popup.PopupSucess("Les contenus ont été réinitialisés avec succès");
    }

    if(element === "media"){
        reset_mod_media();
        popup.PopupSucess("Les images par défauts ont été réinitialisées avec succès");
    }

    functions.saveConfigu();
});