import $ from '/wp-content/plugins/infocob-configu/dist/jquery/jquery.js';
//=================-----Pop Up Template Section------==================//

//--- success
export const PopupEvent = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

export const PopupEdit = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1000,
});

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    showClass: {
        backdrop: 'swal2-noanimation', // disable backdrop animation
        popup: '',                     // disable popup animation
    },
    hideClass: {

    },
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

export function PopupSucess(message){
    Toast.fire({
        icon: 'success',
        title: message
    })
}

//=================-----End Pop Up Template Section------==================//