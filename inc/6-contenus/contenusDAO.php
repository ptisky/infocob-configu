<?php

/**
 * Get content
 * @param $target_dir
 * @param $elemReturned
 */
function get($target_dir, $elemReturned)
{

    $getContenu = file_get_contents($target_dir . '/' . $elemReturned . '.txt');
    $myobj = json_encode(array("contenu" => $getContenu));

    echo $myobj;

    exit;
}

/**
 * Post new content
 * @param $target_dir
 * @param $elemReturned
 */
function post($target_dir, $elemReturned)
{

    $msg = "";

    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }

    $target_file = $target_dir . basename($_FILES["file"]["name"]);


    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_dir . '/' . $elemReturned . '.txt')) {
        chmod($target_dir . "/" . $elemReturned . "." . "txt", 0777);
        $msg = "Le fichier à été téléchargé avec succès";
    } else {
        $msg = "Une erreur est survenu pendant le chargement du fichier";
    }

    echo $msg;
    exit;
}

/**
 * Delete a content
 * @param $target_dir
 * @param $elemReturned
 */
function delete($target_dir, $elemReturned)
{
    $msg = "";

    if (unlink($target_dir . '/' . $elemReturned . '.txt')) {
        $msg = "Le logo à été supprimé avec succès";
    } else {
        $msg = "Une erreur est survenu pendant la suppression du fichier";
    }

    echo $msg;
    exit;
}

/**
 * Preload a content before post
 * @param $target_dir
 * @param $elemReturned
 */
function preload($target_dir, $elemReturned)
{

    $img = $target_dir . '/' . $elemReturned . '.png';

    $getimage = file_get_contents($img);

    $name = basename($img);
    $size = strlen($getimage);
    $type = "txt";

    $myobj = json_encode(array(
        "name" => $name,
        "size" => $size,
        "type" => $type

    ));

    echo $myobj;

    exit;
}

/**
 * Check if a post exist
 * @param $type
 * @return mixed|void
 */
function ifPostExist($type)
{
    if (isset($_POST[$type])) {
        return $_POST[$type];
    } else {
        $msg = "Error " . $type . " is not defined";
        echo $msg;
        exit;
    }
}

$id = ifPostExist("id");
$request = ifPostExist("request");
$elemReturned = ifPostExist("elemReturned");

$target_dir = '../../configurations/configuration_' . $id . '/6-contenus';

if ($request === "POST") {
    post($target_dir, $elemReturned);
}

if ($request === "GET") {
    get($target_dir, $elemReturned);
}

if ($request === "DELETE") {
    delete($target_dir, $elemReturned);
}

if ($request === "PRELOAD") {
    preload($target_dir, $elemReturned);
}





