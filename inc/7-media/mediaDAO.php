<?php

/**
 * Get media
 * @param $target_dir
 * @param $elemReturned
 */
function get($target_dir, $file)
{

    $getimage = file_get_contents($target_dir . '/' . $file["name"]);
    $base64 = 'data:image/png;base64,' . base64_encode($getimage);
    $myobj = json_encode(array("image" => $base64));

    echo $myobj;

    exit;
}

/**
 * Post new media
 * @param $target_dir
 * @param $elemReturned
 */
function post($target_dir, $file, $id_item)
{

    $msg = "";

    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }

    if (move_uploaded_file($file["tmp_name"], $target_dir . "/default_image_" . $id_item . "." ."png")) {
        chmod($target_dir . "/default_image_" . $id_item . "." ."png", 0777);
        $msg = "L'image à été téléchargé avec succès";
    } else {
        $msg = "Une erreur est survenu pendant le chargement de l'image";
    }


    echo $msg;
    exit;
}

/**
 * Delete a media
 * @param $target_dir
 * @param $elemReturned
 */
function delete($target_dir, $file)
{
    $msg = "";

    if (unlink($target_dir . '/' . $file["name"])) {
        $msg = "Le logo à été supprimé avec succès";
    } else {
        $msg = "Une erreur est survenu pendant la suppression du logo";
    }

    echo $msg;
    exit;
}

/**
 * Preload a media before post
 * @param $target_dir
 * @param $elemReturned
 */
function preload($target_dir, $file)
{

    $img = $target_dir . '/' . $file["name"];

    $getimage = file_get_contents($img);

    $name = basename($img);
    $size = strlen($getimage);
    $type = "png";

    $myobj = json_encode(array(
        "name" => $name,
        "size" => $size,
        "type" => $type

    ));

    echo $myobj;

    exit;
}

/**
 * Check if a post exist
 * @param $type
 * @return mixed|void
 */
function ifPostExist($type)
{
    if (isset($_POST[$type])) {
        return $_POST[$type];
    } else {
        $msg = "Error " . $type . " is not defined";
        echo $msg;
        exit;
    }
}


$request = ifPostExist("request");
$id_item = ifPostExist("id_item");
$id_configuration = ifPostExist("id_configuration");
$file = "";

if(isset($_FILES['file']['name'])) {
    $file = $_FILES["file"];
}

$target_dir = '../../configurations/configuration_' . $id_configuration . '/7-media';


if ($request === "POST") {
    post($target_dir, $file, $id_item);
}

if ($request === "GET") {
    get($target_dir, $file);
}

if ($request === "DELETE") {
    delete($target_dir, $file);
}

if ($request === "PRELOAD") {
    preload($target_dir, $file);
}







