import * as popup from '/wp-content/plugins/infocob-configu/js/extra/extra_popup.js';
import * as jsonElements from '/wp-content/plugins/infocob-configu/js/core/core_JsonElements.js';
import * as functions from "/wp-content/plugins/infocob-configu/js/functions.js";

/**
 * Script to get and send mail
 */
export function mail_sender() {

    jQuery(document).ready(function ($) {


        $("#send_mail_container").addClass("active");
        $("body").css("overflow", "hidden");


        $("#close_mail_container").click(function () {
            $("#send_mail_container").removeClass("active");
            $("body").css("overflow", "initial");
        });


        $("#send_mail").click(function () {

            //Get all datas
            const nom_gen = $('#nom_gen').val();
            const tel_gen = $('#tel_gen').val();
            const mail_gen = $('#mail_gen').val();
            const Address_gen = $('#Address_gen').val();
            const ssid_gen = $('#ssid_gen').val();
            const society_name_gen = $('#society_name_gen').val();
            const statut_gen = $('#statut_gen').val();
            const cs_num_gen = $('#cs_num_gen').val();
            const naf_gen = $('#naf_gen').val();
            const tva_gen = $('#tva_gen').val();
            const siege_gen = $('#siege_gen').val();
            const representant_gen = $('#representant_gen').val();

            if (nom_gen && tel_gen && mail_gen && Address_gen && ssid_gen && society_name_gen && statut_gen
                && cs_num_gen && naf_gen && tva_gen && siege_gen && representant_gen) {

                let storedElement = JSON.parse(localStorage.getItem("core_informations"));
                storedElement.nom_site.value = nom_gen;
                storedElement.tel.value = tel_gen;
                storedElement.mail.value = mail_gen;
                storedElement.addr_1.value = Address_gen;
                storedElement.addr_2.value = ssid_gen;
                storedElement.societe.value = society_name_gen;
                storedElement.statut.value = statut_gen;
                storedElement.rcs.value = cs_num_gen;
                storedElement.naf.value = naf_gen;
                storedElement.tva.value = tva_gen;
                storedElement.siege.value = siege_gen;
                storedElement.replegal.value = representant_gen;
                localStorage.setItem("core_informations", JSON.stringify(storedElement));
                functions.saveConfigu();

                try {
                    //array of datas collected
                    let data_email = new FormData();
                    data_email.append("email", mail_gen);
                    data_email.append("telephone", tel_gen);
                    data_email.append("raison_social", society_name_gen);
                    data_email.append("jsonDOC", JSON.stringify(jsonElements.JsonItems()));

                    //ajax request to send datas
                    let request =
                        $.ajax({
                            type: "POST",
                            url: "wp-content/plugins/infocob-configu/inc/core/mail_sender.php",
                            data: data_email,
                            dataType: 'json',
                            timeout: 120000, //2 Minutes
                            cache: false,
                            contentType: false,
                            processData: false,
                        });

                    request.done(function () {
                        popup.PopupEvent.fire({title: 'Merci ! La configuration a bien été envoyé !'});

                        let storedElement = JSON.parse(localStorage.getItem("core_general"));
                        storedElement["mail_send"].value = true;
                        localStorage.setItem("core_general", JSON.stringify(storedElement));
                        functions.saveConfigu();
                    });
                    request.fail(function () {
                        //Code à jouer en cas d'éxécution en erreur du script du PHP

                        popup.PopupEvent.fire({title: "Le mail ne s'est pas envoyé !"});
                    });
                } catch (e) {
                    popup.PopupEvent.fire({title: "Le mail ne s'est pas envoyé !"});
                }
            } else {
                console.log("il manque des champs");
            }
        });

    });
}




