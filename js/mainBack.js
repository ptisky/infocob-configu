let editor;

jQuery(document).ready(function ($) {

    $('#backend_HTML').load('/wp-content/plugins/infocob-configu/template/index_back.html', function () {



        $('#getAllConfigu').DataTable({
            ajax: {
                url: "/wp-content/plugins/infocob-configu/inc/backEnd/getAllConfigu.php",
                dataSrc: 'configurations'
            },
            columns: [
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["id"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["reference"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["date_creation"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["date_update"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["version"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["mail_send"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["wordpress_generator"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: true,
                    'render': function (data, type, row) {
                        return data["general"]["commercial"]["value"]
                    }
                },
                {
                    data: null,
                    orderable: false,
                    'render': function (data, type, row) {
                        return '<button data-id="' + data["general"]["id"]["value"] + '" class="more_btn">Plus</button>'
                    }
                },
                {
                    data: null,
                    orderable: false,
                    'render': function (data, type, row) {
                        return '<button data-id="' + data["general"]["id"]["value"] + '" class="dll_btn">Télécharger</button>'
                    }
                },
                {
                    data: null,
                    orderable: false,
                    'render': function (data, type, row) {
                        return '<button data-id="' + data["general"]["id"]["value"] + '" class="gen_btn">Générer Wordpress</button>'
                    }
                },
                {
                    data: null,
                    orderable: false,
                    'render': function (data, type, row) {
                        return '<button data-id="' + data["general"]["id"]["value"] + '" class="delete_btn">Supprimer</button>'
                    }
                }
            ]

        });

        $('#getAllConfigu tbody').on('click', '.more_btn', function () {

            let idClick = $(this).attr("data-id");

            $.ajax({
                url: location.protocol + "//" + document.location.hostname + "/wp-content/plugins/infocob-configu/inc/backEnd/getAllConfigubyID.php",
                data: {
                    id: idClick
                }
            }).done(function (data) {

                let objPlus = data.configuration[0];

                console.log(objPlus);

                let appendMedias;
                let appendContenus;
                let appendMenusMain;
                let appendMenusFooter;
                let logoHeader;
                let logoFooter;

                if(objPlus.logos.header.value === "true"){
                    logoHeader = location.protocol + "//" + document.location.hostname + "/wp-content/plugins/infocob-configu/configurations/configuration_"+ objPlus.general.id.value +"/1-logo/logo_header.png";
                }else{
                    logoHeader = objPlus.logos.header.value;
                }

                if(objPlus.logos.footer.value === "true"){
                    logoFooter = location.protocol + "//" + document.location.hostname + "/wp-content/plugins/infocob-configu/configurations/configuration_"+ objPlus.general.id.value +"/1-logo/logo_footer.png";
                }else{
                    logoFooter = objPlus.logos.footer.value;
                }

                // Medias
                $.each(objPlus.medias.images, function (index, value) {
                    appendMedias += `<div class="group_back_displayAll"><span class="value_back_list"><img src="${value.image}" class="image_back_displayed"></span></div>`;
                });
                appendMedias = appendMedias.replace('undefined', '');


                //Contenus
                $.each(objPlus.contents, function (index, value) {
                    appendContenus += `<div class="group_back_displayAll"><span class="title_back_list">${value.name} :</span><span class="value_back_list">${value.fill}</span></div>`;
                });
                appendContenus = appendContenus.replace('undefined', '');

                //Menu main
                $.each(objPlus.menus.main, function (index, value) {
                    appendMenusMain += `<div class="group_back_displayAll">
                                            <span class="title_back_list">${index} : </span>
                                            <span class="value_back_list">${value.name}</span>
                                            <span class="value_back_list">Position : ${value.position}</span>
                                        </div>`;
                });
                appendMenusMain = appendMenusMain.replace('undefined', '');

                //Menu footer
                $.each(objPlus.menus.footer, function (index, value) {
                    appendMenusFooter += `<div class="group_back_displayAll">
                                            <span class="title_back_list">${index} : </span>
                                            <span class="value_back_list">${value.name}</span>
                                            <span class="value_back_list">Position : ${value.position}</span>
                                        </div>`;
                });
                appendMenusFooter = appendMenusFooter.replace('undefined', '');

                Swal.fire({
                    title: '<strong>Configuration n*' + objPlus.general.id.value + '</strong>',
                    html: `
                    <ul id="getConfiguBack">
                    
                        <li>
                            <b class="title_back_list">Information générales</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Référence :</span>
                                <span class="value_back_list">${objPlus.general.reference.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Commercial lié :</span>
                                <span class="value_back_list">${objPlus.general.commercial.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Version :</span>
                                <span class="value_back_list">${objPlus.general.version.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Etat du configurateur :</span>
                                <span class="value_back_list">${objPlus.general.state.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Date de création :</span>
                                <span class="value_back_list">${objPlus.general.date_creation.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Date de la dernière modification :</span>
                                <span class="value_back_list">${objPlus.general.date_update.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Mail envoyé ? :</span>
                                <span class="value_back_list">${objPlus.general.mail_send.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Le configuration à t-elle été généré ? :</span>
                                <span class="value_back_list">${objPlus.general.wordpress_generator.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Variable de tutoriel ( non utilisé ) :</span>
                                <span class="value_back_list">${objPlus.general.tutorial.value}</span>
                            </div>
                        </li>
                        
                        <li>
                            <b class="title_back_list">Information de l'entreprise</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Nom du site :</span>
                                <span class="value_back_list">${objPlus.informations.nom_site.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Adresse 1 :</span>
                                <span class="value_back_list">${objPlus.informations.addr_1.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Adresse 2:</span>
                                <span class="value_back_list">${objPlus.informations.addr_2.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Addresse e-mail :</span>
                                <span class="value_back_list">${objPlus.informations.mail.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Numéro de téléphone :</span>
                                <span class="value_back_list">${objPlus.informations.tel.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Nom de la société :</span>
                                <span class="value_back_list">${objPlus.informations.societe.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Code NAF :</span>
                                <span class="value_back_list">${objPlus.informations.naf.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Numéro du registre des commerces :</span>
                                <span class="value_back_list">${objPlus.informations.rcs.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Représentant légal :</span>
                                <span class="value_back_list">${objPlus.informations.replegal.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Adresse du siège :</span>
                                <span class="value_back_list">${objPlus.informations.siege.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Statut de l'entreprise :</span>
                                <span class="value_back_list">${objPlus.informations.statut.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Numéro de TVA :</span>
                                <span class="value_back_list">${objPlus.informations.tva.value}</span>
                            </div>
                        </li>
                    
                        <li>
                            <b class="title_back_list">Logos</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Logo du header :</span>
                                <span class="value_back_list">
                                    <img src="${logoHeader}" class="image_back_displayed">
                                </span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Logo du footer :</span>
                                <span class="value_back_list">
                                    <img src="${logoFooter}" class="image_back_displayed">
                                </span>
                            </div>
                        </li>
                        
                        <li>
                            <b class="title_back_list">Couleurs</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur du header :</span>
                                <span class="value_back_list">${objPlus.colors.header.value}</span>
                                <span class="display_color" style="background: ${objPlus.colors.header.value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur du header au shrink :</span>
                                <span class="value_back_list">${objPlus.colors['header-shrink'].value}</span>
                                <span class="display_color" style="background: ${objPlus.colors['header-shrink'].value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur du footer :</span>
                                <span class="value_back_list">${objPlus.colors.footer.value}</span>
                                <span class="display_color" style="background: ${objPlus.colors.footer.value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur principale :</span>
                                <span class="value_back_list">${objPlus.colors.main.value}</span>
                                <span class="display_color" style="background: ${objPlus.colors.main.value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur principale sombre :</span>
                                <span class="value_back_list">${objPlus.colors["main-dark"].value}</span>
                                <span class="display_color" style="background: ${objPlus.colors["main-dark"].value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur principale claire :</span>
                                <span class="value_back_list">${objPlus.colors["main-light"].value}</span>
                                <span class="display_color" style="background: ${objPlus.colors["main-light"].value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur secondaire :</span>
                                <span class="value_back_list">${objPlus.colors.secondary.value}</span>
                                <span class="display_color" style="background: ${objPlus.colors.secondary.value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur du texte :</span>
                                <span class="value_back_list">${objPlus.colors.text.value}</span>
                                <span class="display_color" style="background: ${objPlus.colors.text.value}"></span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Couleur grise :</span>
                                <span class="value_back_list">${objPlus.colors.grey.value}</span>
                                <span class="display_color" style="background: ${objPlus.colors.grey.value}"></span>
                            </div>
                        </li>
                        
                        <li>
                            <b class="title_back_list">Police</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Police principale :</span>
                                <span class="value_back_list">${objPlus.fonts.main.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Police secondaire :</span>
                                <span class="value_back_list">${objPlus.fonts.sec.value}</span>
                            </div>
                        </li>
                        
                        <li>
                            <b class="title_back_list">Réseaux sociaux</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Facebook :</span>
                                <span class="value_back_list">${objPlus.socials.facebook.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Houzz :</span>
                                <span class="value_back_list">${objPlus.socials.houzz.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Instagram :</span>
                                <span class="value_back_list">${objPlus.socials.instagram.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Linkedin :</span>
                                <span class="value_back_list">${objPlus.socials.linkedin.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Pinterest :</span>
                                <span class="value_back_list">${objPlus.socials.pinterest.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Twitter :</span>
                                <span class="value_back_list">${objPlus.socials.twitter.value}</span>
                            </div>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Youtube :</span>
                                <span class="value_back_list">${objPlus.socials.youtube.value}</span>
                            </div>
                        </li>
                        
                        <li>
                            <b class="title_back_list">Menus</b>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Header :</span>
                                ${appendMenusMain}
                            </div>
                            <br>
                            <div class="group_back_displayAll">
                                <span class="title_back_list">Footer :</span>
                                ${appendMenusFooter}
                            </div>
                        </li>
                        
                        <li>
                            <b class="title_back_list">Contenus</b>
                            ${appendContenus}
                        </li>
                        
                        <li>
                            <b class="title_back_list">Medias</b>
                            <div class="contenu_list">
                                ${appendMedias}
                            </div>
                        </li>
                    </ul>
                    
                    <button id="load_this_configu" data-id="${objPlus.general.id.value}">Charger cette configuration</button>
                    `,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                })

                $( "#load_this_configu" ).click(function() {
                    localStorage.setItem("core_general", JSON.stringify(objPlus.general));
                    localStorage.setItem("core_informations", JSON.stringify(objPlus.informations));
                    localStorage.setItem("mod_logo", JSON.stringify(objPlus.logos));
                    localStorage.setItem("mod_color", JSON.stringify(objPlus.colors));
                    localStorage.setItem("mod_font", JSON.stringify(objPlus.fonts));
                    localStorage.setItem("mod_social", JSON.stringify(objPlus.socials));
                    localStorage.setItem("mod_menu", JSON.stringify(objPlus.menus));
                    localStorage.setItem("mod_contenus", JSON.stringify(objPlus.contents));
                    localStorage.setItem("mod_media", JSON.stringify(objPlus.medias));

                    Toast.fire({
                        icon: 'success',
                        title: "Vous avez chargé cette configuration"
                    });

                    window.open(location.origin, '_blank');
                });
            });
        });

        $('#getAllConfigu tbody').on('click', '.dll_btn', function () {

            let idClick = $(this).attr("data-id");

            $.ajax({
                'async': false,
                'global': false,
                'url': '/wp-content/plugins/infocob-configu/inc/dllConfiguFile.php',
                data: {
                    id: idClick
                },
                'success': function (data) {
                    Toast.fire({
                        icon: 'success',
                        title: "Cette configuration à bien été téléchargé"
                    });
                }
            });

        });

        $('#getAllConfigu tbody').on('click', '.delete_btn', function () {

            let element = $(this);

            let idClick = $(this).attr("data-id");

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success bootno',
                    cancelButton: 'btn btn-danger bootno'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Veux-tu vraiment supprimer ?',
                text: "Cette action sera irréversible.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Oui',
                cancelButtonText: 'Non, annuler',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    swalWithBootstrapButtons.fire(
                        'Supprimé !',
                        'La configuration a été supprimé',
                        'success'
                    )

                    $.ajax({
                        url: "/wp-content/plugins/infocob-configu/inc/backEnd/deleteConfigu.php",
                        data: {
                            id: idClick
                        }
                    }).done(function (data) {
                        element.parent().parent().remove();
                        Toast.fire({
                            icon: 'success',
                            title: "La configuration a été supprimé"
                        })
                    });

                } else if (

                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Conservé',
                        "La configuration n'a pas été supprimé",
                        'error'
                    )
                }
            })


        });

        $("#backend_HTML").on('click', '#delete_allConfig', function () {
            $.ajax({
                url: "/wp-content/plugins/infocob-configu/inc/backEnd/deleteAll.php",
            }).done(function (data) {
                Toast.fire({
                    icon: 'success',
                    title: "Les configurations ont été supprimés"
                });
                location.reload();
            });
        });
    });

    $.ajax({
        url: location.protocol + "//" + document.location.hostname + "/wp-content/plugins/infocob-configu/inc/backEnd/getAllConfigu.php",
        success: function(data) {

            let configurations_running = 0;
            let configurations_finished = 0;
            let configurations_generate = 0;

            $.each(data["configurations"], function(i, item) {

                if(item.general.reference.value){
                    $("#selectRef").append(`<option value="${item.general.reference.value}">${item.general.reference.value}</option>`);

                    $("#generate_button").click(function(){
                        let reference_selected = $('#selectRef').find(":selected").text();
                        if(reference_selected !== "Référence"){
                            let input = $('.link_gene_configu input');
                            input.val(location.protocol + "//" + document.location.hostname + '?import=' + reference_selected);
                            input.focus();
                            input.select();
                            document.execCommand('copy');
                        }
                    });
                }


                //Increment configuration
                configurations_running = configurations_running + 1;

                //Increment configuration finished
                if(item.general.mail_send.value === true){
                    configurations_finished = configurations_finished + 1;
                }

                //Increment configuration finished
                if(item.general.wordpress_generator.value === true){
                    configurations_generate = configurations_generate + 1;
                }
            });

            $(".result_config_saved").append(configurations_running);
            $(".result_config_finished").append(configurations_finished);
            $(".result_config_generate").append(configurations_generate);
        }
    });
});


function download(filename, text) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    customClass: {
        popup: 'backPlusContainer',
        container: 'backPlusContainer',
    },
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})