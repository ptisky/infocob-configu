import * as displayAll from '/wp-content/plugins/infocob-configu/js/core/core_displayAll.js';
import * as popup from '/wp-content/plugins/infocob-configu/js/extra/extra_popup.js';
import $ from '/wp-content/plugins/infocob-configu/dist/jquery/jquery.js';

/**
 * Import script
 */
export function pre_importConfig() {
    Swal.fire({
        title: 'Es-tu sur de vouloir faire ça ?',
        text: "Cela supprimera ta configuration en cours",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, continuer',
        cancelButtonText: 'NON ! Annuler'
    }).then((result) => {
        if (result.isConfirmed) {
            const file = getsaveConfig.files[0];
            if (!!file) {
                const reader = new FileReader();
                reader.readAsText(file, "gbk");
                reader.onload = function () {
                    let tmp1 = this.result;
                    const newData = JSON.parse(tmp1);

                    //set in current localstorage
                    localStorage.setItem("core_general", JSON.stringify(newData.general));
                    localStorage.setItem("core_informations", JSON.stringify(newData.informations));
                    localStorage.setItem("mod_logo", JSON.stringify(newData.logos));
                    localStorage.setItem("mod_color", JSON.stringify(newData.colors));
                    localStorage.setItem("mod_font", JSON.stringify(newData.fonts));
                    localStorage.setItem("mod_social", JSON.stringify(newData.socials));
                    localStorage.setItem("mod_menu", JSON.stringify(newData.menus));
                    localStorage.setItem("mod_contenus", JSON.stringify(newData.contents));
                    localStorage.setItem("mod_media", JSON.stringify(newData.medias));

                    //display contents
                    displayAll.getConfigLS();
                }

                $('#getsaveConfig').val('');
                popup.PopupEvent.fire({title: "Votre fichier a été importé !"})
                location.reload();
            }

        } else if (result.dismiss === Swal.DismissReason.cancel) {
            $('#getsaveConfig').val('');

            popup.PopupEvent.fire({title: "Votre fichier n'a pas été importé"})
        }
    })
}
