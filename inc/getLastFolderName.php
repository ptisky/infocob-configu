<?php

/**
 * Get last folder name
 */

if ($_GET["id"]) {

    $files = scandir('../configurations/configuration_' . $_GET["id"] . '/posts');

    $folderName = "";

    foreach ($files as $file) {

        $folderName = $file;

    }

    header('Content-Type: application/json');
    echo json_encode(array('name' => $folderName));
}

?>