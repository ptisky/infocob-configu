import $ from '/wp-content/plugins/infocob-configu/dist/jquery/jquery.js';

//==--- LocalStorage Default values ---==//

export const default_logo = {
    header: {value: $(".logo-container img").attr("src")},
    footer: {value: $("#footer .footer-logo img").attr("src")}
};

export const default_color = {
    main: {value: "#e3ac06"},
    "main-light": {value: "#ebc655"},
    "main-dark": {value: "#b0902f"},
    secondary: {value: "#000000"},
    grey: {value: "#f0f0f0"},
    text: {value: "#211e1f"},
    header: {value: "#211e1f"},
    'header-shrink': {value: "#211e1f"},
    footer: {value: "#211e1f"},
};

export const default_font = {
    main: {value: eval(getComputedStyle(document.documentElement).getPropertyValue('--infocobweb-font-main'))},
    sec: {value: eval(getComputedStyle(document.documentElement).getPropertyValue('--infocobweb-font-sec'))}
}

export const default_social = {
    facebook: {value: "true"},
    twitter: {value: "true"},
    youtube: {value: "true"},
    linkedin: {value: "true"},
    instagram: {value: "false"},
    pinterest: {value: "false"},
    houzz: {value: "false"},
}

export const default_menu = {
    main: {
        0: {
            'id': 1,
            'name': 'Accueil',
            'position': 1,
            'state': "checked",
            'default': 1,
            'url': '/'
        },
        1: {
            'id': 2,
            'name': 'Société',
            'position': 2,
            'state': "checked",
            'default': 1,
            'url': '/societe'
        },
        2: {
            'id': 3,
            'name': 'Prestations',
            'position': 3,
            'state': "checked",
            'default': 0,
            'url': '/prestations'
        },
        3: {
            'id': 4,
            'name': 'Projets réalisés',
            'position': 4,
            'state': "checked",
            'default': 0,
            'url': '/projet'
        },
        4: {
            'id': 5,
            'name': 'Actualités',
            'position': 5,
            'state': "checked",
            'default': 1,
            'url': '/actualites'
        },
        5: {
            'id': 6,
            'name': 'Recrutement',
            'position': 6,
            'state': "checked",
            'default': 0,
            'url': '/offre-emploi'
        },
        6: {
            'id': 7,
            'name': 'Contact',
            'position': 7,
            'state': "checked",
            'default': 1,
            'url': '/contact'
        },
        7: {
            'id': 8,
            'name': 'Espace client',
            'position': 8,
            'state': "checked",
            'default': 0,
            'url': 'https://espaceclient.infocob-solutions.com'
        },
        8: {
            'id': 9,
            'name': 'Projets complets',
            'position': 301,
            'state': "checked",
            'default': 0,
            'url': '/prestations/projets-complets'
        },
        9: {
            'id': 10,
            'name': 'Extension',
            'position': 302,
            'state': "checked",
            'default': 0,
            'url': '/prestations/extension'
        },
        10: {
            'id': 11,
            'name': 'Rénovation',
            'position': 303,
            'state': "checked",
            'default': 0,
            'url': '/prestations/renovation'
        }
    },
    footer: {
        0: {
            'id': 1,
            'name': 'Accueil',
            'position': 1,
            'state': "checked",
            'default': 1,
            'url': '/'
        },
        1: {
            'id': 2,
            'name': 'Société',
            'position': 2,
            'state': "checked",
            'default': 0,
            'url': '/societe'
        },
        2: {
            'id': 3,
            'name': 'Prestations',
            'position': 3,
            'state': "checked",
            'default': 0,
            'url': '/prestations'
        },
        3: {
            'id': 4,
            'name': 'Projets réalisés',
            'position': 4,
            'state': "checked",
            'default': 0,
            'url': '/projet'
        },
        4: {
            'id': 5,
            'name': 'Actualités',
            'position': 5,
            'state': "checked",
            'default': 0,
            'url': '/actualites'
        },
        5: {
            'id': 6,
            'name': 'Recrutement',
            'position': 6,
            'state': "checked",
            'default': 0,
            'url': '/offre-emploi'
        },
        6: {
            'id': 7,
            'name': 'Contact',
            'position': 7,
            'state': "checked",
            'default': 0,
            'url': '/contact'
        },
        7: {
            'id': 8,
            'name': 'Espace client',
            'position': 8,
            'state': "checked",
            'default': 0,
            'url': 'https://espaceclient.infocob-solutions.com'
        },
        8: {
            'id': 9,
            'name': 'Politique de confidentialité',
            'position': 9,
            'state': "checked",
            'default': 0,
            'url': '/politique-de-confidentialite'
        },
        9: {
            'id': 10,
            'name': 'Mentions légales',
            'position': 10,
            'state': "checked",
            'default': 0,
            'url': '/mentions-legales'
        },
        10: {
            'id': 11,
            'name': 'Plan du site',
            'position': 11,
            'state': "checked",
            'default': 0,
            'url': '/plan-du-site'
        }
    }
}

export const default_contenus = {
    1:{
        name: "Accueil",
        slug: "accueil",
        fill: false,
    },
    2:{
        name: "Société",
        slug: "societe",
        fill: false,
    },
    3:{
        name: "Prestations",
        slug: "prestations",
        fill: false,
    },
    4:{
        name: "Projets réalisés",
        slug: "projets_realises",
        fill: false,
    },
    5:{
        name: "Actualités",
        slug: "actualites",
        fill: false,
    },
    6:{
        name: "Recrutement",
        slug: "recrutement",
        fill: false,
    },
    7:{
        name: "Contact",
        slug: "contact",
        fill: false,
    },
    8:{
        name: "Espace client",
        slug: "espace_client",
        fill: false,
    },
    9:{
        name: "Politique de confidentialité",
        slug: "politique_de_confidentialite",
        fill: false,
    },
    10:{
        name: "Mentions légales",
        slug: "mentions_legales",
        fill: false,
    },
    11:{
        name: "Plan du site",
        slug: "plan_du_site",
        fill: false,
    }
}

export const default_media = {
    images : {
        0 : {
            id : 1,
            image : "/wp-content/uploads/2020/08/75136-scaled.jpg"
        },
        1 : {
            id : 2,
            image : "/wp-content/uploads/2020/08/home-hero-1500x630-1.jpg"
        },
        2 : {
            id : 3,
            image : "/wp-content/uploads/2020/08/FIRE-SAFETY-SOLUTIONS.jpg"
        },
        3 : {
            id : 4,
            image : "/wp-content/uploads/2020/08/Slider-7.18.jpg"
        },
        4 : {
            id : 5,
            image : "/wp-content/uploads/2020/07/maison-en-bois-a-lorient-realisee-par-ambiose-56.jpg"
        },
        5 : {
            id : 6,
            image : "/wp-content/uploads/2020/07/maison_bois.jpeg"
        },
    },
    videos : {

    }
}

//==--- End LocalStorage Default values ---==//

//==--- DOM Default values ---==//
export const dom_logo_header_source = $('.logo-container source');
export const dom_logo_header_img = $('.logo-container img');
export const dom_logo_footer_source = $('.footer-logo source');
export const dom_logo_footer_img = $('.footer-logo img');
//==--- End DOM Default values ---==//