import * as popup from '/wp-content/plugins/infocob-configu/js/extra/extra_popup.js';
import * as jsonElements from '/wp-content/plugins/infocob-configu/js/core/core_JsonElements.js';

/**
 * Export script
 */
export function exportAll() {
    let currentId = JSON.parse(localStorage.getItem("core_general")).id.value;
    let id = parseInt(currentId);

    let dataExported = jsonElements.JsonItems();
    let json = JSON.stringify(dataExported);
    let blob = new Blob([json]);
    let link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = "Demo1_Configu" + id + ".json";
    link.click();

    popup.PopupEvent.fire({title: "Un fichier à été téléchargé"});
}