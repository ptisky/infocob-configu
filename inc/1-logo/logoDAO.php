<?php

/**
 * Get logo
 * @param $target_dir
 * @param $elemReturned
 */
function get($target_dir, $elemReturned)
{

    $getimage = file_get_contents($target_dir . '/' . $elemReturned . '.png');
    $base64 = 'data:image/png;base64,' . base64_encode($getimage);
    $myobj = json_encode(array("image" => $base64));

    echo $myobj;

    exit;
}

/**
 * Post new logo
 * @param $target_dir
 * @param $elemReturned
 */
function post($target_dir, $elemReturned)
{

    var_dump($_FILES);

    $tmpHandle = tmpfile();
    $metaDatas = stream_get_meta_data($tmpHandle);
    $tmpFilename = $metaDatas['uri'];
    fclose($tmpHandle);

    var_dump($tmpFilename);

    $msg = "";

    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }


    if (move_uploaded_file($tmpFilename, $target_dir . '/' . $elemReturned . '.png')) {
        chmod($target_dir . "/" . $elemReturned . "." . "png", 0777);
        $msg = "Le logo à été téléchargé avec succès";
    } else {
        $msg = "Une erreur est survenu pendant le chargement du logo";
    }

    echo $msg;
    exit;
}

/**
 * Delete a logo
 * @param $target_dir
 * @param $elemReturned
 */
function delete($target_dir, $elemReturned)
{
    $msg = "";

    if (unlink($target_dir . '/' . $elemReturned . '.png')) {
        $msg = "Le logo à été supprimé avec succès";
    } else {
        $msg = "Une erreur est survenu pendant la suppression du logo";
    }

    echo $msg;
    exit;
}

/**
 * Preload a logo before post
 * @param $target_dir
 * @param $elemReturned
 */
function preload($target_dir, $elemReturned)
{

    $img = $target_dir . '/' . $elemReturned . '.png';

    $getimage = file_get_contents($img);

    $name = basename($img);
    $size = strlen($getimage);
    $type = "png";

    $myobj = json_encode(array(
        "name" => $name,
        "size" => $size,
        "type" => $type

    ));

    echo $myobj;

    exit;
}

/**
 * Check if a post exist
 * @param $type
 * @return mixed|void
 */
function ifPostExist($type)
{
    if (isset($_POST[$type])) {
        return $_POST[$type];
    } else {
        $msg = "Error " . $type . " is not defined";
        echo $msg;
        exit;
    }
}

$id = ifPostExist("id");
$request = ifPostExist("request");
$elemReturned = ifPostExist("elemReturned");

$target_dir = '../../configurations/configuration_' . $id . '/1-logo';

if ($request === "POST") {
    post($target_dir, $elemReturned);
}

if ($request === "GET") {
    get($target_dir, $elemReturned);
}

if ($request === "DELETE") {
    delete($target_dir, $elemReturned);
}

if ($request === "PRELOAD") {
    preload($target_dir, $elemReturned);
}

