<?php

/**
 * Send mail script
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../dist/PHPMailer/src/Exception.php';
require '../../dist/PHPMailer/src/PHPMailer.php';
require '../../dist/PHPMailer/src/SMTP.php';

try {

    header('Content-Type: application/json');

    $myfile = fopen("../../template/configuration.json", "w") or die("Unable to open file!");
    fwrite($myfile, $_POST["jsonDOC"]);
    fclose($myfile);

    $mail = new PHPMailer(true);

    $mail->setFrom('contact@configu.local', 'Infocob web'); // Personnaliser l'envoyeur
    $mail->addAddress('m.roy@infocob-solutions.com', 'moi'); // Ajouter le destinataire
    $mail->addReplyTo('m.roy@infocob-solutions.com', 'Information'); // L'adresse de réponse
    $mail->addCC('m.roy@infocob-solutions.com');
    $mail->addBCC('m.roy@infocob-solutions.com');

    $mail->addAttachment('../configurations/configuration.json', 'configuration.json'); // Ajouter un attachement
    $mail->isHTML(true); // Paramétrer le format des emails en HTML ou non

    $mail->Subject = 'Configuration de theme demo infocob web';
    $mail->Body = '<!doctype html>
                    <html lang="fr">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport"
                              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Configuration de theme demo infocob web</title>
                        <style>
                        a {
                            color: #7a7a7a;
                        }
                        </style>
                    </head>
                    <body>
                    <table width=100% cellspacing=0 cellpadding=0 border=0>
                        <tr>
                            <td align=center>
                                <table width=100% cellspacing=0 cellpadding=0 border=0 style="max-width: 600px;">
                                 
                                    <tr>
                                        <td align=center>
                                            <table width=100% bgcolor=white cellspacing=0 cellpadding=0 border=0 style="border-radius:0px; padding: 15px; border:1px solid #dcdcdc; font-family: sans-serif;">
                                                <tr>
                                                    <td colspan=2 align=center bgcolor=#000000 style="border-radius:0px; padding: 15px; color:#ffffff;">
                                                        <span style="font-size: 20px;font-weight: bold;">Configuration de theme demo infocob web</span><br><br>
                                                        <span style="opacity: 0.7; font-size: 16px;">Contact</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 15px">
                                                        <table width=100% cellspacing=3 cellpadding=7 border=0>
                    
                   
                                                            <tr valign="top">
                                                                <td align=right style="font-size: 16px;">
                                                                    <b>Email</b>
                                                                </td>
                                                                <td align=left style="background: #fafafa; border-radius:0px; font-size: 16px;">
                                                                    <a href="mailto:' . $_POST["email"] . '" style="color: #7a7a7a;">' . $_POST["email"] . '</a>
                                                                </td>
                                                            </tr>
                    
                    
                    
                    
                                                            <tr valign="top">
                                                                <td align=right style="font-size: 16px;">
                                                                    <b>Téléphone</b>
                                                                </td>
                                                                <td align=left style="background: #fafafa; border-radius:0px; font-size: 16px;">
                                                                    <a href="tel:' . $_POST["telephone"] . '" style="color: #7a7a7a;">' . $_POST["telephone"] . '</a>
                                                                </td>
                                                            </tr>
                    
                    
                    
                    
                                                            <tr valign="top">
                                                                <td align=right style="font-size: 16px;">
                                                                    <b>Raison sociale</b>
                                                                </td>
                                                                <td align=left style="background: #fafafa; border-radius:0px; font-size: 16px;">
                                                                    ' . $_POST["raison_social"] . '
                                                                </td>
                                                            </tr>
                    
                                                            <tr>
                                                                <td align=right style="color: green">
                                                                    <b>&#10004;</b>
                                                                </td>
                                                                <td align=left style="font-size: 14px">
                                                                    Accepte la politique de confidentialité (RGPD)
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align=center style="font-size: 14px; padding: 30px">
                                            <a href="mailto:techniqueweb@infocob-solutions.com" style="color: #7a7a7a;">Infocob #web</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                    </body>
                    </html>';

    /* Finally send the mail. */
    $mail->send();
    echo "Mail has been sent successfully!";

    $Output = array(
        "error" => false,
        "message" => "N/A",
        "output" => "N/A"
    );


} catch (\Throwable $e) {
    $Output["error"] = true;
    $Output["message"] = $e->getMessage();

} finally {
    echo json_encode($Output, JSON_FORCE_OBJECT);
    die();
}

?>