import * as functions from '/wp-content/plugins/infocob-configu/js/functions.js';

export const version = "1.0";
console.log("Configurateur version : " + version);

// Animate loader off screen
jQuery(window).load(function() {
    jQuery(".se-pre-con").fadeOut("slow");
});

//======================----------START CONFIGURATOR-----------========================//
jQuery(document).ready(function ($) {

    functions.referenceTest();

    functions.checkIfUserConnected();

    functions.configuScript();

});
//======================----------END CONFIGURATOR-----------========================//
