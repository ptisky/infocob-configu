import * as functions from "/wp-content/plugins/infocob-configu/js/functions.js";

/**
 * Verify if element is different from the default value
 * @param path
 * @param itemStorage
 * @param baseValue
 */
export function checkUpdate(path, itemStorage, baseValue) {

    if (localStorage.getItem(itemStorage) !== JSON.stringify(baseValue)) {
        addUpdate(path);
    } else {
        removeUpdate(path);
    }
}

/**
 * Add class update
 * @param path
 */
function addUpdate(path) {
    path.addClass("updated");
    functions.setDateUpdate();
}

/**
 * Remove class update
 * @param path
 */
function removeUpdate(path) {
    if (path.hasClass("updated")) {
        path.removeClass("updated");
        functions.setDateUpdate();
    }
}