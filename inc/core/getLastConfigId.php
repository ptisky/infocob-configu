<?php

/**
 * Get last configuration id
 */

$files = scandir('../../configurations/');

$folderID = "";

foreach ($files as $file) {

    $split = explode("configuration_", $file);
    if($split[0] === "" && $split[1]){
        if($folderID < $split[1]){
            $folderID = $split[1];
        }
    }
}

header('Content-Type: application/json');
echo json_encode(array('id' => $folderID));