import * as mod_logo from "/wp-content/plugins/infocob-configu/js/modules/mod_logo.js";
import * as mod_color from "/wp-content/plugins/infocob-configu/js/modules/mod_color.js";
import * as mod_font from "/wp-content/plugins/infocob-configu/js/modules/mod_font.js";
import * as mod_social from "/wp-content/plugins/infocob-configu/js/modules/mod_social.js";
import * as mod_menu from "/wp-content/plugins/infocob-configu/js/modules/mod_menu.js";
import * as mod_contenus from "/wp-content/plugins/infocob-configu/js/modules/mod_contenus.js";
import * as mod_media from "/wp-content/plugins/infocob-configu/js/modules/mod_media.js";

/**
 * Get & display logo module
 */
export function get_mod_Logo(){
    mod_logo.runProcessLogo("header");
    mod_logo.runProcessLogo("footer");
}

/**
 * Get & display color module
 */
export function get_mod_color(){
    mod_color.getColor("main");
    mod_color.getColor("main-light");
    mod_color.getColor("main-dark");
    mod_color.getColor("secondary");
    mod_color.getColor("grey");
    mod_color.getColor("text");
    mod_color.getColor("header");
    mod_color.getColor("header-shrink");
    mod_color.getColor("footer");
}

/**
 * Get & display font module
 */
export function get_mod_font(){
    mod_font.getFont("main");
    mod_font.getFont("sec");
}

/**
 * Get & display social module
 */
export function get_mod_social(){
    mod_social.getSocial("facebook");
    mod_social.getSocial("twitter");
    mod_social.getSocial("youtube");
    mod_social.getSocial("linkedin");
    mod_social.getSocial("instagram");
    mod_social.getSocial("pinterest");
    mod_social.getSocial("houzz");
}

/**
 * Get & display menu module
 */
export function get_mod_menu(){
    mod_menu.getMenu("main");
    mod_menu.getMenu("footer");
}

/**
 * Get & display content module
 */
export function get_mod_contenus(){
    mod_contenus.getContenus();
}

/**
 * Get & display media module
 */
export function get_mod_media(){
    mod_media.getMedia("images");
    mod_media.getMedia("videos");
}

/**
 * Get & display all modules
 */
export function getConfigLS() {
    get_mod_Logo();
    get_mod_color();
    get_mod_font();
    get_mod_social();
    get_mod_menu();
    get_mod_contenus();
    get_mod_media();
}