import $ from '/wp-content/plugins/infocob-configu/dist/jquery/jquery.js';

//Open popup
export function openConf() {
    $(".popup-demo-psion").show();
    $("#container").addClass("infocob-configu_active");
    $("#footer").addClass("infocob-configu_active");
}

//Close popup
export function closeConf() {
    $("#container").removeClass("infocob-configu_active");
    $("#footer").removeClass("infocob-configu_active");
    $(".popup-demo-psion").hide();
}